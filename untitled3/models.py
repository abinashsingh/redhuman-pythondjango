import datetime
from django.db import models
from django.contrib.auth.models import User


class BloodMember(models.Model):
   user_id = models.ForeignKey(User)
   #id = models.IntegerField()
   #username = models.ForeignKey(User)
   #email = models.CharField(max_length=100)
   fullname = models.CharField(max_length=100, null=True)
   gender = models.CharField(max_length=100, null=True)
   relationship = models.CharField(max_length=100)
   bloodgroup = models.CharField(max_length=100)
   address = models.CharField(max_length=100)
   phone = models.CharField(max_length=100)


class BloodEvent(models.Model):
   user_id = models.ForeignKey(User)
   #username = models.ForeignKey(User)
   #email = models.CharField(max_length=100)
   eventname = models.CharField(max_length=100, null=True)
   dateevent = models.DateField(null=True)

class BloodAlertt(models.Model):
   user_id = models.ForeignKey(User)
   #username = models.ForeignKey(User)
   message = models.CharField(max_length=800)
   nbloodgroup = models.CharField(max_length=100,null=False)
   contactdesc = models.CharField(max_length=300,null=False)
   deadlinedate = models.DateField(null=True)
   updateddate = models.DateField(null=False, blank=False, auto_now=True)
   #message = models.IntegerField(null=True)




