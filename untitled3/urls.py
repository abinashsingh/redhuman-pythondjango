"""untitled3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views
#from untitled3.loginforms import LoginForm
#from django.contrib.auth.views import password_change_done, password_change, password_reset_done
from untitled3.hello import reset, reset_confirm, success
from untitled3.views import register_page, main_page, logout_page, home_form, login, blood_form, add_mem, search_page, \
    add_event, showbucketlist, editdata, Delete_Data, Delete_Dataa, change_password, send_alert

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^register/$', register_page),
   # url(r'^$', views.login, {'template_name': 'login.html',  'authentication_form': LoginForm}, name='login'),
    url(r'^$', login,  name='login'),
    url(r'^dashboard/$', main_page),
   # url(r'^passwordchange/$', name='password_change'),
   # url(r'^user/(\w+)/$', user_page),
   # url(r'^accounts/password/reset/done/$',
       # password_reset_done,
        #{'template_name': 'registration/password_reset_done.html'},
        #name="password_reset_done"),
    #url(r'^accounts/password/change/$', password_change, {
     #   'template_name': 'registration/password_change_form.html'},
      #  name='password_change'),
    #url(r'^accounts/password/change/done/$', password_change_done,
     #   {'template_name': 'registration/password_change_done.html'},
      #  name='password_change_done'),
    url(r'^resetform/$', reset, name='reset'),

    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', reset_confirm, name='password_reset_confirm'),
    url(r'^success/$', success, name='success'),


    url(r'^logout/$', logout_page),
    url(r'^home/', home_form),
    url(r'^addform/$', blood_form),
    url(r'^addmember/$', add_mem),
    url(r'^search/$', search_page),
    url(r'^addevent/$', add_event),
    url(r'^showbucket/$', showbucketlist),
    url(r'^addalert/$', send_alert),
    url(r'^update/', editdata, name="editdata"),
    url(r'^delete/(?P<id>[\w|\W]+)$', Delete_Data, name="delete_location"),
    url(r'^remove/(?P<id>[\w|\W]+)$', Delete_Dataa, name="delete"),
    url(r'^password/$', change_password, name='change_password')


] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
