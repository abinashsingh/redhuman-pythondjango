#import json

from django.contrib import messages
#from django.core import serializers
from django.contrib.auth.forms import PasswordChangeForm
from django.db.models import Count
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.template import RequestContext, Template
from untitled3.forms import *
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import logout, login, update_session_auth_hash
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login as contrib_login
from untitled3 import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.views import password_reset, password_reset_confirm
from untitled3.forms import RegistrationForm
from untitled3.models import BloodMember, BloodEvent, BloodAlertt
from django.contrib.auth.models import User
import re
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django import template
register = template.Library()


@csrf_exempt
def register_page(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'], password=form.cleaned_data['password1'], email=form.cleaned_data['email'])
            return HttpResponseRedirect('/')

    else:
     form = RegistrationForm()
    variables = RequestContext(request, {'form': form})
    print(variables)
    return render_to_response('signup.html', variables)
    #return render(request, "signup.html", {'form': form})



@login_required(login_url="/")
def main_page(request):
    dataa = BloodMember.objects.filter(bloodgroup='O+').count()
    datab = BloodMember.objects.filter(bloodgroup='O-').count()
    datac = BloodMember.objects.filter(bloodgroup='A+').count()
    datad = BloodMember.objects.filter(bloodgroup='A-').count()
    datae = BloodMember.objects.filter(bloodgroup='B+').count()
    dataf = BloodMember.objects.filter(bloodgroup='B-').count()
    datag = BloodMember.objects.filter(bloodgroup='AB+').count()
    datah = BloodMember.objects.filter(bloodgroup='AB-').count()
    total = BloodMember.objects.all().count()
    if total==0:
      data1=0
      data2=0
      data3=0
      data4=0
      data5=0
      data6=0
      data7=0
      data8=0

    else:
      data1= float(float(dataa)*100/float(total))
      data2= float(float(datab)*100/float(total))
      data3= float(float(datac)*100/float(total))
      data4= float(float(datad)*100/float(total))
      data5= float(float(datae)*100/float(total))
      data6= float(float(dataf)*100/float(total))
      data7= float(float(datag)*100/float(total))
      data8= float(float(datah)*100/float(total))


   # mydata=dataa.replace("'","")
      datab = BloodAlertt.objects.all()
      #trackb = User.objects.filter(username=request.user.username)
      #return render(request, "main_page.html", {'username': request.user.username, 'datab': datab, 'trackb': trackb})

    return render(request, "main_page.html",{'username':request.user.username, 'datab': datab, 'data1':data1, 'data2':data2, 'data3':data3,'data4':data4,'data5':data5,'data6':data6,'data7':data7,'data8':data8, 'total':total})

#def main_page(request):
 #   template = Template('main_page.html')
  #  context = {
     #   'username': request.user.username,
   # }
    #return HttpResponse(template.render(context, request))





def logout_page(request): # this is used to sign out the logged in user
    logout(request)
    return HttpResponseRedirect('/')

@login_required(login_url="/")
def home_form(request):  #redirects to template to add tracking name for the geneological tree
    return render(request, "home.html", {'username':request.user.username})
@login_required(login_url="/")
def blood_form(request):  #redirects to template to add tracking name for the geneological tree
    data= BloodMember.objects.filter(user_id=request.user)
    track= User.objects.filter(username=request.user.username)
    return render(request, "add_blood.html", {'username':request.user.username, 'data': data, 'track': track})
    #return render(request, "add_blood.html", {'username':request.user.username})

def login(request):

    if request.user.is_authenticated():

      return redirect(settings.LOGIN_REDIRECT_URL)

    return contrib_login(request)

#@login_required(login_url="/")
#def main_page_load(request):
 #   datab = BloodAlertt.objects.all()
  #  trackb = User.objects.filter(username=request.user.username)
   # return render(request, "main_page.html", {'username': request.user.username, 'datab': datab, 'trackb': trackb})



        # mydata=dataa.replace("'","")




#@login_required(login_url="/")
#def form_template(request):
 #data= BloodMember.objects.all()
 #track= auth_user.objects.filter(username=request.user.username)
 #return render(request, "mainform.html", {'username':request.user.username, 'data': data, 'track': track})
@login_required(login_url="/")
def add_mem(request):
    if request.method == 'GET':
       data= BloodMember.objects.all()
       track= User.objects.filter(username=request.user.username)
       return render(request, "add_blood.html", {'username':request.user.username, 'data': data, 'track': track})
    #if request.method == 'POST':
    else:
        form = MemberForm(request.POST)
        if form.is_valid(): # <<<< Correct!
            user_id = request.user
            #user_id = User.objects.get(id=user_id)
            fullname = form.cleaned_data['fullname']
            gender = form.cleaned_data['gender']
            relationship = form.cleaned_data['relationship']
            bloodgroup = form.cleaned_data['bloodgroup']
            address = form.cleaned_data['address']
            phone = form.cleaned_data['phone']


            if(relationship=='Relationship'):
                messages.error(request, "Required fields must not left blank.")
               # return render(request, "main_page.html", {'form':form, 'data': data})            elif(bloodgroup=='Blood Group'):
                return HttpResponseRedirect('/addform/')
            elif(bloodgroup=='Blood Group'):
                messages.error(request, "Required fields must not left blank.")
               # return render(request, "main_page.html", {'form':form, 'data': data})            elif(bloodgroup=='Blood Group'):
                return HttpResponseRedirect('/addform/')


            else:
                bloodmember = BloodMember(user_id=user_id, fullname=fullname,gender=gender, relationship=relationship, bloodgroup=bloodgroup, address=address, phone=phone)
                bloodmember.save()
                return HttpResponseRedirect('/addform/')


        else:
            data= BloodMember.objects.all()
            messages.error(request, "Required fields must not left blank.")
            return render(request, "main_page.html", {'form':form, 'data': data})


@login_required(login_url="/")
def search_page(request): # this is also used for filter data from database with userdefined input

    if request.method == 'GET':
       dataa = BloodMember.objects.filter(bloodgroup='O+').count()
       datab = BloodMember.objects.filter(bloodgroup='O-').count()
       datac = BloodMember.objects.filter(bloodgroup='A+').count()
       datad = BloodMember.objects.filter(bloodgroup='A-').count()
       datae = BloodMember.objects.filter(bloodgroup='B+').count()
       dataf = BloodMember.objects.filter(bloodgroup='B-').count()
       datag = BloodMember.objects.filter(bloodgroup='AB+').count()
       datah = BloodMember.objects.filter(bloodgroup='AB-').count()
       total = BloodMember.objects.all().count()
       data1= float(float(dataa)*100/float(total))
       data2= float(float(datab)*100/float(total))
       data3= float(float(datac)*100/float(total))
       data4= float(float(datad)*100/float(total))
       data5= float(float(datae)*100/float(total))
       data6= float(float(dataf)*100/float(total))
       data7= float(float(datag)*100/float(total))
       data8= float(float(datah)*100/float(total))
       datab = BloodAlertt.objects.all()

       # mydata=dataa.replace("'","")

       return render(request, "main_page.html",{'username':request.user.username, 'datab':datab, 'data1':data1, 'data2':data2, 'data3':data3,'data4':data4,'data5':data5,'data6':data6,'data7':data7,'data8':data8, 'total':total})

    else:
        bloodgroup = request.POST['bloodgroup']
        data = BloodMember.objects.filter(bloodgroup=bloodgroup)
        datab = BloodAlertt.objects.all()

    return render(request, "main_page.html", {'data': data,'datab': datab})

@login_required(login_url="/")
def add_event(request):
    if request.method == 'GET':
       dataevent= BloodEvent.objects.all()
       track= User.objects.filter(username=request.user.username)
       return render(request, "main_page.html", {'username':request.user.username, 'dataevent': dataevent, 'track': track})
    #if request.method == 'POST':
    else:
        form = EventForm(request.POST)
        if form.is_valid(): # <<<< Correct!
            user_id = request.user
            #user_id = User.objects.get(id=user_id)
            eventname = form.cleaned_data['eventname']
            dateevent = form.cleaned_data['dateevent']


            bloodevent = BloodEvent(user_id=user_id, eventname=eventname, dateevent=dateevent)
            bloodevent.save()
            return HttpResponseRedirect('/addevent/')
        else:
            dataevent= BloodEvent.objects.all()
            messages.error(request, "Required fields must not left blank.")
            return render(request, "main_page.html", {'form':form, 'dataevent': dataevent})


@login_required(login_url="/")
def showbucketlist(request):  #redirects to template to add tracking name for the geneological tree
    data= BloodEvent.objects.filter(user_id=request.user)
    return render(request, "showbucket.html", {'username':request.user.username, 'data': data})
    #return render(request, "add_blood.html", {'username':request.user.username})
@login_required(login_url="/")
def editdata(request):
    id = BloodEvent.cleaned_data['id']

    instance = get_object_or_404(BloodEvent, id=id)
    form = PostForm(request.POST or None, instance=instance)
    if form.is_valid():
         instance=form.save(commit=False)
         instance.save()

    context={
        "eventname":instance.eventname,

        "instance":instance,
        "form": form

    }

    return render(request,"add_blood.html",context)

@login_required(login_url="/")
def Delete_Data(request, id): #this is used to delete data from database
    delt = BloodMember.objects.filter(id=id).filter(user_id = request.user).delete()
    #treedetail = park.relation
    messages.error(request, "Success.")

    return HttpResponseRedirect('/addform/')

@login_required(login_url="/")
def Delete_Dataa(request, id): #this is used to delete data from database
    delt = BloodEvent.objects.filter(id=id).filter(user_id = request.user).delete()
    #treedetail = park.relation
    messages.error(request, "Success.")
    return HttpResponseRedirect('/showbucket/')

@login_required(login_url="/")
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            #messages.success(request, 'Your password was successfully updated!')
            return HttpResponseRedirect('/logout/')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/change_password.html', {
        'form': form
    })

@login_required(login_url="/")
def send_alert(request):
    if request.method == 'GET':
       dataalert= BloodAlertt.objects.all()
       track= User.objects.filter(username=request.user.username)
       return render(request, "main_page.html", {'username':request.user.username, 'dataalert': dataalert, 'track': track})
    #if request.method == 'POST':
    else:
        form = BloodAlerttForm(request.POST)
        if form.is_valid(): # <<<< Correct!
            user_id = request.user
            #user_id = User.objects.get(id=user_id)
            message = form.cleaned_data['message']
            nbloodgroup = form.cleaned_data['nbloodgroup']
            contactdesc = form.cleaned_data['contactdesc']
            deadlinedate = form.cleaned_data['deadlinedate']


            bloodalert = BloodAlertt(user_id=user_id, message=message, nbloodgroup=nbloodgroup, contactdesc=contactdesc, deadlinedate=deadlinedate)
            bloodalert.save()
            return HttpResponseRedirect('/dashboard/')
        else:
            dataalert= BloodAlertt.objects.all()
            messages.error(request, "Required fields must not left blank.")
            return render(request, "main_page.html", {'form':form, 'dataalert': dataalert})
