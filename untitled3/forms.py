import re
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import datetime
from django.forms import ModelForm
from untitled3 import settings
from untitled3.models import BloodMember, BloodEvent


class RegistrationForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30, widget=forms.TextInput(attrs={'class': 'form-control', 'autofocus': 'True'}))
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(label='Password (Again)', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password1 == password2:
                return password2

        raise forms.ValidationError('Passwords do not match.')



    def clean_username(self):
        username = self.cleaned_data['username']
        if not re.search(r'^\w+$', username):
            raise forms.ValidationError('Username can only contain alphanumeric characters and the underscore.')
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username
        raise forms.ValidationError('Username is already taken.')



class MemberForm(forms.Form):                                  # this class is used to add family member details
   user_id = forms.IntegerField()
   fullname = forms.CharField(max_length=100)
   gender = forms.CharField(max_length=100)
   relationship = forms.CharField(max_length=100)
   bloodgroup = forms.CharField(max_length=100)
   address = forms.CharField(max_length=100)
   phone = forms.CharField(max_length=100)




class EventForm(forms.Form):                                  # this class is used to add family member details
   user_id = forms.IntegerField()
  # email = forms.CharField(max_length=100)
   eventname = forms.CharField(max_length=100)
   dateevent = forms.DateField(input_formats=['%m/%d/%Y'], initial=datetime.date.today)


class PostForm(ModelForm):
    class Meta:
        model = BloodEvent
        fields = '__all__'

class BloodAlerttForm(forms.Form):
   user_id = forms.IntegerField()
   #username = models.ForeignKey(User)
   message = forms.CharField(max_length=800)
   nbloodgroup = forms.CharField(max_length=100)
   contactdesc = forms.CharField(max_length=300)
   deadlinedate = forms.DateField(input_formats=['%m/%d/%Y'], initial=datetime.date.today)

